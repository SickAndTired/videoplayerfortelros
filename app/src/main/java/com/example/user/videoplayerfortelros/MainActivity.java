package com.example.user.videoplayerfortelros;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;
import com.halilibo.bettervideoplayer.subtitle.CaptionsView;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    private BetterVideoPlayer bvp;
    Button refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.background_activity_button).setOnClickListener(view -> startActivity(new Intent(MainActivity.this, BackgroundActivity.class)));

        findViewById(R.id.fulscreen_activity_button).setOnClickListener(view -> startActivity(new Intent(MainActivity.this, FullscreenActivity.class)));

        refresh = findViewById(R.id.refresh);
        if (savedInstanceState != null) {
            refresh.setVisibility(View.VISIBLE);
        }

        refresh.setOnClickListener(view -> {
            if (savedInstanceState != null) {

                savedInstanceState.clear();
                bvp.setAutoPlay(true);
                bvp.setSource(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));
                bvp.setCaptions(R.raw.sub, CaptionsView.CMime.SUBRIP);
            }
        });

        bvp = findViewById(R.id.bvp);

        if (savedInstanceState == null) {
            bvp.setAutoPlay(true);
            bvp.setSource(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));
            bvp.setCaptions(R.raw.sub, CaptionsView.CMime.SUBRIP);
        }


        bvp.setHideControlsOnPlay(true);

        bvp.getToolbar().inflateMenu(R.menu.menu_dizi);
        bvp.getToolbar().setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_enable_swipe:
                    bvp.enableSwipeGestures(getWindow());
                    break;
                case R.id.action_enable_double_tap:
                    bvp.enableDoubleTapGestures(5000);
                    break;
                case R.id.action_disable_swipe:
                    bvp.disableGestures();
                    break;
                case R.id.action_show_bottombar:
                    bvp.setBottomProgressBarVisibility(true);
                    break;
                case R.id.action_hide_bottombar:
                    bvp.setBottomProgressBarVisibility(false);
                    break;
                case R.id.action_show_captions:
                    bvp.setCaptions(R.raw.sub, CaptionsView.CMime.SUBRIP);
                    break;
                case R.id.action_hide_captions:
                    bvp.removeCaptions();
            }
            return false;
        });

        bvp.enableSwipeGestures(getWindow());

        bvp.setCallback(new BetterVideoCallback() {
            @Override
            public void onStarted(BetterVideoPlayer player) {
                Timber.d("Started");
            }

            @Override
            public void onPaused(BetterVideoPlayer player) {
                Timber.d("Paused");
            }

            @Override
            public void onPreparing(BetterVideoPlayer player) {
                Timber.d("Preparing");
            }

            @Override
            public void onPrepared(BetterVideoPlayer player) {
                Timber.d("Prepared");
            }

            @Override
            public void onBuffering(int percent) {
                Timber.d("Buffering %s", percent);
            }

            @Override
            public void onError(BetterVideoPlayer player, Exception e) {
                Timber.d("Error : %s", e.getMessage());
            }

            @Override
            public void onCompletion(BetterVideoPlayer player) {
                Timber.d("Completed");
            }

            @Override
            public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

            }
        });
    }

    @Override
    public void onPause() {
        bvp.pause();
        super.onPause();
    }
}
