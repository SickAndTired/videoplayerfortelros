package com.example.user.videoplayerfortelros;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.halilibo.bettervideoplayer.BetterVideoPlayer;

public class FullscreenActivity extends AppCompatActivity {

    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler handler = new Handler();
    private BetterVideoPlayer betterVideoPlayer;
    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            betterVideoPlayer.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable runnable1 = () -> {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }
    };
    private boolean visible;
    private final Runnable hideRunnable = this::hide;

    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener mDelayHideTouchListener = (view, motionEvent) -> {
        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS);
        }
        return false;
    };

    @SuppressLint("PrivateResource")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        visible = true;
        betterVideoPlayer = findViewById(R.id.bvp);
        betterVideoPlayer.setSource(Uri.parse("android.resource://" + getPackageName() + "/" + com.example.user.videoplayerfortelros.R.raw.video));
        betterVideoPlayer.getToolbar().setTitle(R.string.full_screen);
        betterVideoPlayer.getToolbar()
                .setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        betterVideoPlayer.getToolbar().setNavigationOnClickListener(v -> onBackPressed());

        betterVideoPlayer.setOnClickListener(view -> toggle());
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        delayedHide(100);
    }

    private void toggle() {
        if (visible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        visible = false;

        handler.removeCallbacks(runnable1);
        handler.postDelayed(runnable, UI_ANIMATION_DELAY);
    }

    private void show() {
        betterVideoPlayer.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        visible = true;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable1, UI_ANIMATION_DELAY);
    }

    private void delayedHide(int delayMillis) {
        handler.removeCallbacks(hideRunnable);
        handler.postDelayed(hideRunnable, delayMillis);
    }
}
